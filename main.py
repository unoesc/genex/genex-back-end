from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from routers.users import user
from routers.area import area
from routers.gene import gene_controller
from routers.gene import semi
from routers.animals import animal
from routers.animals import animal_history
from routers import reports

app = FastAPI(
    title="Inbov API",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        'http://localhost:8080',
        'http://localhost:8081',
        'http://localhost:8000',
        'http://18.234.81.62:8000',
        'http://duducdi.com:8000',
        'http://duducdi.com:8080',
        'http://18.234.81.62:8080',
    ],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

# app.add_exception_handler(500, errors_handler.handler)
app.include_router(user.router, tags=["User"])
app.include_router(area.router, tags=["Area"])
app.include_router(gene_controller.router, tags=["GeneController"])
app.include_router(semi.router, tags=["Semi"])
app.include_router(animal.router, tags=["Animal"])
app.include_router(animal_history.router, tags=["AnimalHistory"])
app.include_router(reports.router, tags=["Reports"])

