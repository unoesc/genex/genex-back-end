import auth
from fastapi import APIRouter, HTTPException, status, Depends
import model
import schema
from typing import List
from utils import get_random_string


router = APIRouter()


@router.post('/area')
async def create_area(area_data: schema.PostArea, access=Depends(auth.authenticate)):
    user, session = access
    area = model.Area(
        name=area_data.name,
        description=area_data.description,
        user_id=user.id,
    )
    session.add(area)
    session.flush()
    if area_data.private_gene is not None:
        gene = model.GeneController(
            area_id=area.id,
            active=True,
            nitrogen_LT_status=area_data.private_gene.nitrogen_LT_status,
            nitrogen_day_loss_tax=area_data.private_gene.nitrogen_day_loss_tax,
            nitrogen_next_refill=area_data.private_gene.nitrogen_next_refill,
            nitrogen_last_refill=area_data.private_gene.nitrogen_last_refill,
            sensor_token=get_random_string(10, '1234567890'),
        )
    else:
        gene = model.GeneController(
            area_id=area.id,
            active=False,
            sensor_token=get_random_string(10, '1234567890'),
        )
    session.add(gene)
    session.commit()
    session.close()
    return


@router.get('/user-areas', response_model=List[schema.AreaGet])
async def get_user_areas(access=Depends(auth.authenticate)):
    user, session = access
    areas = user.areas

    areas = [schema.AreaGet.from_orm(area) for area in areas]

    session.close()
    return areas


@router.get('/area/{area_id}', response_model=schema.AreaGet, responses={
    404: {"description": "Area not found"},
})
async def get_area(area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")
    area = schema.AreaGet.from_orm(area)
    session.close()
    return area


@router.patch('/area', responses={
    404: {"description": "Area not found"},
})
async def update_area(area_data: schema.AreaPatch, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_data.id).first()

    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")

    area.name = area_data.name if area_data.name is not None else area.name
    area.description = area_data.description if area_data.description is not None else area.description
    if area_data.private_gene is not None:
        gene_controller = area.gene_controller.first()
        gene_controller.active = True
        gene_controller.nitrogen_LT_status = area_data.private_gene.nitrogen_LT_status
        gene_controller.nitrogen_day_loss_tax = area_data.private_gene.nitrogen_day_loss_tax
        gene_controller.nitrogen_next_refill = area_data.private_gene.nitrogen_next_refill
        gene_controller.nitrogen_last_refill = area_data.private_gene.nitrogen_last_refill
    else:
        gene_controller = area.gene_controller.first()
        gene_controller.active = False

    session.add(gene_controller)
    session.add(area)
    session.commit()
    session.close()
    return


@router.delete('/area/{area_id}', responses={
    404: {"description": "Area not found"},
})
async def delete_area(area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_id).first()

    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")

    session.delete(area)
    session.commit()
    session.close()
    return
