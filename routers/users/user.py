import auth
from fastapi import APIRouter, HTTPException, status, Response, Depends
import model
import schema
import db
from typing import Optional


router = APIRouter()


def verify_user_email(session, email: str) -> Optional[model.User]:
    return session.query(model.User).filter_by(email=email).first()


@router.post('/create-user', responses={
    409: {"description": "Email already registered"},
})
async def create_user(user_data: schema.PostUser, response: Response):
    session = db.connect()
    user = verify_user_email(session, user_data.email.strip().lower())
    if user:
        session.close()
        raise HTTPException(status.HTTP_409_CONFLICT, detail="Email already registered")
    user = model.User(
        email=user_data.email.strip().lower(),
        name=user_data.name,
        phone=user_data.phone,
        password=user_data.password
    )
    session.add(user)
    session.commit()
    payload = await auth.create_token(response, user)
    session.close()
    return payload


@router.put('/login', responses={
    404: {"description": "User not found"},
    401: {"description": "Wrong password"},
})
async def login(user_data: schema.LoginUser, response: Response):
    session = db.connect()
    user = verify_user_email(session, user_data.email.strip().lower())
    if user is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found")
    if user.password != user_data.password:
        session.close()
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="Wrong password")
    payload = await auth.create_token(response, user)
    session.close()
    return payload


@router.get('/user', response_model=schema.GetUser)
async def get_user(access=Depends(auth.authenticate)):
    user, session = access
    return schema.GetUser.from_orm(user)


@router.patch('/user', responses={
    409: {"description": "Email already registered"},
})
async def update_user(user_data: schema.UserPatch, access=Depends(auth.authenticate)):
    user, session = access
    if user_data.email is not None:
        new_user = verify_user_email(session, user_data.email.strip().lower())
        if new_user and user.id != new_user.id:
            session.close()
            raise HTTPException(status.HTTP_409_CONFLICT, detail="Email already registered")
        user.email = user_data.email.strip().lower()

    user.name = user_data.name if user_data.name is not None else user.name
    user.phone = user_data.phone if user_data.phone is not None else user.phone
    session.commit()
    session.close()
    return


