import calendar
from datetime import date, datetime
from sqlalchemy import func
import auth
from fastapi import APIRouter, HTTPException, status, Depends
import model
import schema

router = APIRouter()


@router.get("/reports/{area_id}/{month}", responses={
    404: {'description': "Area not found"},
})
async def get_reports(area_id: int, month: date, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")

    filter_min_date = datetime(month.year, month.month, 1)
    filter_max_date = datetime(month.year, month.month, calendar.monthrange(month.year, month.month)[1])

    report = schema.GetReport(
        cows=0,
        min_date=filter_min_date,
        repetitions=0,
        inseminations=0,
        missed_fertilizations=0,
        pregnants=0,
        cow_drys=0,
        cow_lactations=0,
        cow_fattening=0,
        used_semis=[],
    )

    if area.animals.first() is None:
        session.close()
        return report

    min_date = area.animals.order_by(model.Animal.created_datetime.asc()).first().created_datetime

    report.min_date = date(min_date.year, min_date.month, 1)

    animals = area.animals.join(
        model.AnimalHistory,
    ).filter(
        model.AnimalHistory.created_datetime >= filter_min_date,
        model.AnimalHistory.created_datetime <= filter_max_date,
    )

    for animal in animals:
        stats = animal.history.filter(
            model.AnimalHistory.created_datetime >= filter_min_date,
            model.AnimalHistory.created_datetime <= filter_max_date,
        )
        for s in stats:
            if s.status == model.AnimalHistory.Status.repeating:
                report.repetitions += 1
            elif s.status == model.AnimalHistory.Status.inseminated:
                report.inseminations += 1
            elif s.status == model.AnimalHistory.Status.missed_fertilization:
                report.missed_fertilizations += 1

    total_animals = area.animals.filter(
        model.Animal.created_datetime <= filter_max_date,
    )
    report.cows = total_animals.count()
    for ani in total_animals:
        last_status = ani.history.filter(
            model.AnimalHistory.created_datetime >= filter_min_date,
            model.AnimalHistory.created_datetime <= filter_max_date,
        ).order_by(model.AnimalHistory.created_datetime.desc()).first().status
        if last_status == model.AnimalHistory.Status.dry:
            report.cow_drys += 1
        elif last_status == model.AnimalHistory.Status.lactation:
            report.cow_lactations += 1
        elif last_status == model.AnimalHistory.Status.fattening:
            report.cow_fattening += 1
        elif last_status == model.AnimalHistory.Status.pregnant:
            report.pregnants += 1

    semis_names = session.query(
        model.Semi.breed,
        func.count(
            model.Semi.breed
        ).label("counts")
    ).filter(
        model.Semi.gene_controller_id == area_id,
        model.Semi.used_datetime >= filter_min_date,
        model.Semi.used_datetime <= filter_max_date,
    ).group_by(
        model.Semi.breed
    ).all()

    for name in semis_names:
        semis = session.query(
            model.Semi
        ).filter(
            model.Semi.breed == name.breed,
            model.Semi.used_datetime >= filter_min_date,
            model.Semi.used_datetime <= filter_max_date,
        ).all()
        report.used_semis.append(
            schema.SemisGet.custom_from_orm(
                name.breed,
                semis
            )
        )

    session.close()
    return report
