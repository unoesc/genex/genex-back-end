import auth
from fastapi import APIRouter, HTTPException, status, Depends
import schema
from datetime import datetime, date

router = APIRouter()


@router.patch("/gene-controller", responses={
    404: {"description": "Gene controller not found"},
})
async def edit_gene_controller(data: schema.GeneControllerPatch, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter_by(id=data.area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Gene controller not found")

    gene_controller = area.gene_controller.first()
    gene_controller.nitrogen_LT_status = data.nitrogen_LT_status
    gene_controller.nitrogen_day_loss_tax = data.nitrogen_day_loss_tax
    gene_controller.nitrogen_next_refill = data.nitrogen_next_refill
    gene_controller.nitrogen_last_refill = datetime.now()
    session.add(gene_controller)
    session.commit()
    session.close()
    return


@router.get("/gene-controller/{area_id}", response_model=schema.GeneControllerGet, responses={
    404: {"description": "Gene controller not found"},
})
async def get_gene_controller(area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter_by(id=area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Gene controller not found")

    gene_controller = schema.GeneControllerGet.from_orm(
        area.gene_controller.first(),
    )

    session.close()
    return gene_controller


