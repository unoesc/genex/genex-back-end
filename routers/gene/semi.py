from sqlalchemy import func
import auth
from fastapi import APIRouter, HTTPException, status, Depends
import model
import schema
from typing import List

router = APIRouter()


@router.get("/semis/{area_id}", response_model=List[schema.SemisGet], responses={
    404: {'description': "Area not found"},
})
async def get_semis(area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    semis_names = session.query(
        model.Semi.breed,
        func.count(
            model.Semi.breed
        ).label("counts")
    ).filter(
        model.Semi.gene_controller_id == area_id,
        model.Semi.used_datetime == None
    ).group_by(
        model.Semi.breed
    ).all()
    response = []

    for name in semis_names:
        semis = session.query(model.Semi).filter_by(breed=name.breed, used_datetime=None).all()
        response.append(schema.SemisGet.custom_from_orm(name.breed, semis))

    session.close()
    return response


@router.post("/semis", responses={
    404: {'description': "Area not found"},
})
async def create_semis(semis_data: schema.PostSemis, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == semis_data.area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")

    for i in range(semis_data.quantity):
        semi = model.Semi(
            breed=semis_data.breed,
            gene_controller_id=area.id,
            bull_id=semis_data.bull_id,
        )
        session.add(semi)
    session.commit()
    session.close()
    return


