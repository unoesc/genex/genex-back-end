from sqlalchemy import or_
from unidecode import unidecode
import auth
from fastapi import APIRouter, HTTPException, status, Depends
import model
import schema


router = APIRouter()


@router.post("/animal", responses={
    404: {"description": "Area not found"},
    409: {"description": "Animal already exists"},
})
async def create_animal(data: schema.Animal, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter_by(id=data.area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")
    animal = session.query(model.Animal).filter(model.Animal.id == data.id).first()
    if animal is not None:
        session.close()
        raise HTTPException(status.HTTP_409_CONFLICT, "Animal already exists")
    animal = model.Animal(
        area_id=data.area_id,
        id=data.id,
        name=data.name,
    )
    session.add(animal)
    session.flush()
    animal_history = model.AnimalHistory(
        animal_id=animal.id,
    )
    session.add(animal_history)
    session.commit()
    session.close()
    return


@router.patch("/animals/", response_model=schema.GetAnimals)
async def get_animals(data: schema.AnimalsFilter, access=Depends(auth.authenticate)):
    user, session = access
    animals = user.areas.filter_by(id=data.area_id).first().animals

    filters = []
    for animal in animals:
        last_status = model.AnimalHistory.Status.to_portuguese(animal.history.order_by(
                model.AnimalHistory.created_datetime.desc()
            ).first().status)
        if last_status not in filters:
            filters.append(last_status)

    if data.text is not None:
        data.text = unidecode(data.text).lower()
        animals = animals.filter(
            or_(
                model.Animal.id.ilike(f"%{data.text}%"),
                model.Animal.name.ilike(f"%{data.text}%"),
            )
        )

    response = schema.GetAnimals.custom_from_orm(animals, filters, data.current_status)
    session.close()
    return response


@router.delete("/animal/{area_id}/{id}", responses={
    404: {"description": "Animal not found"},
})
async def delete_animal(area_id: int, id: str, access=Depends(auth.authenticate)):
    user, session = access
    animal = user.areas.filter_by(id=area_id).first().animals.filter_by(id=id).first()
    if animal is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Animal not found")
    session.delete(animal)
    session.commit()
    session.close()
    return

