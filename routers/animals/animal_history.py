import auth
from fastapi import APIRouter, HTTPException, status, Depends
import model
import schema
from typing import List
from datetime import datetime, timedelta, date

router = APIRouter()


def get_available_options(session, animal):
    animal = session.query(model.Animal).filter(model.Animal.id == animal.id).first()
    status = animal.history.order_by(model.AnimalHistory.created_datetime.desc()).first().status
    if status == model.AnimalHistory.Status.unknown:
        data = []
        for s in model.AnimalHistory.Status:
            if s != model.AnimalHistory.Status.unknown:
                data.append(model.AnimalHistory.Status.to_portuguese(s))
        return data
    elif status == model.AnimalHistory.Status.inseminated:
        data = [model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.pregnant),
                model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.dry),
                model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.in_heat),
                model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.inseminated),
                ]
        return data
    elif status == model.AnimalHistory.Status.pregnant:
        data = [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.missed_fertilization),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.lactation),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.fattening),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.dry),
        ]
        return data
    elif status == model.AnimalHistory.Status.lactation:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.inseminated),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.in_heat),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.fattening),
        ]
    elif status == model.AnimalHistory.Status.dry:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.missed_fertilization),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.lactation),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.fattening),
        ]
    elif status == model.AnimalHistory.Status.repeating:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.inseminated),
        ]
    elif status == model.AnimalHistory.Status.in_heat:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.inseminated),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.pregnant),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.lactation),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.dry),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.missed_fertilization),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.repeating),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.fattening),
        ]
    elif status == model.AnimalHistory.Status.missed_fertilization:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.lactation),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.fattening),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.in_heat),
        ]
    else:
        return [
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.inseminated),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.in_heat),
            model.AnimalHistory.Status.to_portuguese(model.AnimalHistory.Status.lactation),
        ]


def setAnimalHistory(session, animal: model.Animal, data: schema.AlterHistory) -> model.AnimalHistory:
    animal = session.query(model.Animal).filter(model.Animal.id == animal.id).first()
    if model.AnimalHistory.Status.from_portuguese(data.status) == model.AnimalHistory.Status.inseminated:
        history = animal.history.filter(
            model.AnimalHistory.created_datetime > (datetime.now() - timedelta(days=90))
        ).order_by(
            model.AnimalHistory.created_datetime.desc()
        )
        inseminated = None
        missed_fertilization = None
        lactation = None
        for s in history:
            if s.status == model.AnimalHistory.Status.inseminated:
                inseminated = s
            elif s.status == model.AnimalHistory.Status.missed_fertilization:
                missed_fertilization = s
        if inseminated is not None and missed_fertilization is not None:
            if inseminated.created_datetime > missed_fertilization.created_datetime:
                repeating = model.AnimalHistory(
                    animal_id=animal.id,
                    created_datetime=(datetime.now() - timedelta(seconds=1)),
                    status=model.AnimalHistory.Status.repeating,
                )
                session.add(repeating)
        elif inseminated is not None and missed_fertilization is None:
            repeating = model.AnimalHistory(
                animal_id=animal.id,
                created_datetime=(datetime.now() - timedelta(seconds=1)),
                status=model.AnimalHistory.Status.repeating,
            )
            session.add(repeating)

        if data.private_semi_id is not None:
            semi = session.query(model.Semi).filter_by(id=data.private_semi_id).first()
            semi.used_datetime = datetime.now()
            session.add(semi)

        animal_history = model.AnimalHistory(
            animal_id=animal.id,
            created_datetime=datetime.now(),
            status=model.AnimalHistory.Status.from_portuguese(data.status),
            private_semi_id=data.private_semi_id,
            public_semi_breed=data.public_semi_breed,
            public_semi_bull_id=data.public_semi_bull_id,
        )

        return animal_history

    elif model.AnimalHistory.Status.from_portuguese(data.status) in [
        model.AnimalHistory.Status.pregnant,
        model.AnimalHistory.Status.dry,
        model.AnimalHistory.Status.in_heat
    ] and animal.history.order_by(model.AnimalHistory.created_datetime.desc()).first().status in [
        model.AnimalHistory.Status.pregnant,
        model.AnimalHistory.Status.inseminated,
        model.AnimalHistory.Status.dry,
    ]:
        last_history = animal.history.order_by(model.AnimalHistory.created_datetime.desc()).first()
        animal_history = model.AnimalHistory(
            animal_id=animal.id,
            created_datetime=datetime.now(),
            status=model.AnimalHistory.Status.from_portuguese(data.status),
            private_semi_id=last_history.private_semi_id,
            public_semi_breed=last_history.public_semi_breed,
            public_semi_bull_id=last_history.public_semi_bull_id,
        )
        return animal_history
    elif model.AnimalHistory.Status.from_portuguese(data.status) == model.AnimalHistory.Status.missed_fertilization:
        last_history = animal.history.order_by(model.AnimalHistory.created_datetime.desc()).first()
        animal_history = model.AnimalHistory(
            animal_id=animal.id,
            created_datetime=datetime.now(),
            status=model.AnimalHistory.Status.from_portuguese(data.status),
            private_semi_id=last_history.private_semi_id,
            public_semi_breed=last_history.public_semi_breed,
            public_semi_bull_id=last_history.public_semi_bull_id,
        )
        return animal_history
    else:
        animal_history = model.AnimalHistory(
            animal_id=animal.id,
            created_datetime=datetime.now(),
            status=model.AnimalHistory.Status.from_portuguese(data.status),
            private_semi_id=data.private_semi_id,
            public_semi_breed=data.public_semi_breed,
            public_semi_bull_id=data.public_semi_bull_id,
        )
        return animal_history


@router.patch("/history-get/", response_model=schema.GetHistories, responses={
    404: {'description': "Animal not found"},
})
async def get_history(data: schema.HistoryFilter, access=Depends(auth.authenticate)):
    user, session = access

    animal = session.query(
        model.Animal
    ).join(
        model.Area
    ).filter(
        model.Animal.id == data.animal_id,
        model.Area.user_id == user.id,
    ).first()

    if animal is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Animal not found")

    initial_date = animal.history.order_by(model.AnimalHistory.created_datetime).first().created_datetime
    histories = animal.history.order_by(model.AnimalHistory.created_datetime.desc())

    filters = []
    for h in histories:
        if model.AnimalHistory.Status.to_portuguese(h.status) not in filters:
            filters.append(model.AnimalHistory.Status.to_portuguese(h.status))

    if data.status is not None:
        histories = histories.filter(
            model.AnimalHistory.status == model.AnimalHistory.Status.from_portuguese(data.status))

    if data.start_date is not None:
        histories = histories.filter(model.AnimalHistory.created_datetime >= data.start_date)

    response = schema.GetHistories.custom_from_orm(histories, initial_date, filters)
    session.close()
    return response


@router.patch("/history-edit/", responses={
    404: {'description': "Animal not found"},
    400: {'description': "Area not found"},
})
async def edit_history(data: schema.AlterHistory, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == data.area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Area not found")

    animal = area.animals.filter(model.Animal.id == data.animal_id).first()

    if animal is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Animal not found")

    new_history = setAnimalHistory(session, animal, data)
    session.add(new_history)
    session.commit()
    session.close()
    return


@router.get("/history-options/{animal_id}/{area_id}", response_model=List[str], responses={
    404: {'description': "Animal not found"},
    400: {'description': "Area not found"},
})
async def get_history_options(animal_id: str, area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Area not found")

    animal = area.animals.filter(model.Animal.id == animal_id).first()
    if animal is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Animal not found")

    response = get_available_options(session, animal)
    session.close()
    return response


@router.get("/history-events/{area_id}", response_model=List[schema.HistoryEvents], responses={
    404: {'description': "Area not found"},
})
async def get_history_events(area_id: int, access=Depends(auth.authenticate)):
    user, session = access
    area = user.areas.filter(model.Area.id == area_id).first()
    if area is None:
        session.close()
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Area not found")

    response = []

    gene_controller = area.gene_controller.first()
    if gene_controller.active:
        current_status = gene_controller.nitrogen_LT_status - (
                (date.today() - gene_controller.nitrogen_last_refill).days * gene_controller.nitrogen_day_loss_tax)
        if current_status < 10:
            response.append(schema.HistoryEvents(
                type="nitrogen",
                title="Nítrogênio a baixo dos 10 Litros",
                occurrence_date=gene_controller.nitrogen_next_refill,
                text="O nitrogênio está a baixo dos 10 Litros. Clique aqui para atualizar o status do nitrogênio.",
            )
            )
        elif gene_controller.nitrogen_next_refill is not None and date.today() > gene_controller.nitrogen_next_refill:
            response.append(schema.HistoryEvents(
                type="nitrogen",
                title="O próximo refil de nitrogênio está atrasado",
                occurrence_date=gene_controller.nitrogen_next_refill,
                text="O próximo refil de nitrogênio está atrasado. Clique aqui e atualize a data do próximo refil.",
            )
        )
        elif gene_controller.nitrogen_next_refill is not None and (gene_controller.nitrogen_next_refill - date.today()).days <= 10:
            response.append(schema.HistoryEvents(
                type="nitrogen",
                title="O próximo refil de nitrogênio está próximo ",
                occurrence_date=gene_controller.nitrogen_next_refill,
                text="O próximo refil de nitrogênio está próximo. Clique aqui para atualizar o status do nitrogênio.",
            )
        )

        if gene_controller.semis.filter(model.Semi.used_datetime == None).count() <= 10:
            response.append(schema.HistoryEvents(
                type="semi",
                title="Há poucos sêmens disponíveis",
                occurrence_date=None,
                text="Há poucos sêmens disponíveis. Clique aqui para adicionar sêmens.",
            )
            )

    animals = area.animals
    for animal in animals:
        last_history = animal.history.order_by(model.AnimalHistory.created_datetime.desc()).first()
        if last_history.status == model.AnimalHistory.Status.inseminated:
            if (datetime.now() - last_history.created_datetime).days >= 90:
                response.append(schema.HistoryEvents(
                    type="animal",
                    title="Inseminação fecundada",
                    occurrence_date=date((last_history.created_datetime + timedelta(days=90)).year,
                                         (last_history.created_datetime + timedelta(days=90)).month,
                                         (last_history.created_datetime + timedelta(days=90)).day),
                    text="O período de fecundação do animal foi concluído. Clique aqui para atualizar o status do animal para 'Em gestação'.",
                    animal=schema.GetAnimal.custom_from_orm(animal)
                )
                )
        elif last_history.status == model.AnimalHistory.Status.pregnant:
            if (datetime.now() - last_history.created_datetime).days >= 90:
                response.append(schema.HistoryEvents(
                    type="animal",
                    title="Período de secagem",
                    occurrence_date=date((last_history.created_datetime + timedelta(days=180)).year,
                                         (last_history.created_datetime + timedelta(days=180)).month,
                                         (last_history.created_datetime + timedelta(days=180)).day),
                    text="O período de secagem do animal está próximo. Clique aqui para atualizar o status do animal para 'Seca'.",
                    animal=schema.GetAnimal.custom_from_orm(animal)
                )
                )
        elif last_history.status == model.AnimalHistory.Status.dry:
            if (datetime.now() - last_history.created_datetime).days >= 260:
                response.append(schema.HistoryEvents(
                    type="animal",
                    title="Animal próximo do parto",
                    occurrence_date=date((last_history.created_datetime + timedelta(days=270)).year,
                                         (last_history.created_datetime + timedelta(days=270)).month,
                                         (last_history.created_datetime + timedelta(days=270)).day),
                    text="O animal está próximo do parto. Caso ele ja tanha parido, clique aqui para atualizar o status do animal para 'Lactação'.",
                    animal=schema.GetAnimal.custom_from_orm(animal)
                )
                )

    session.close()
    return response
