from fastapi import APIRouter, HTTPException, status
import db
import model


router = APIRouter()


@router.post("/sensor-status", responses={
    401: {'description': "Unauthorized"},
})
async def post_sensor_status(sensor_token: str, lt: int):
    session = db.connect()

    gene_controller = session.query(model.GeneController).filter_by(sensor_token=sensor_token).first()
    if gene_controller is None:
        session.close()
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, "Unauthorized")

    gene_controller.nitrogen_LT_status = lt
    session.add(gene_controller)
    session.commit()
    session.close()
    return

