#!/usr/bin/env bash

./venv/bin/uvicorn main:app --host 0.0.0.0 --reload
