import time
import schedule
from routines.fake_sensor import sensor_job


print("Starting routines")
schedule.every(30).seconds.do(sensor_job)

while True:
    schedule.run_pending()
    time.sleep(1)
