import db
from datetime import datetime
import model
import random


def get_random_value():
    return random.random() * 100


def sensor_job():
    print("Sensor job called")
    if datetime.now().minute % 2 == 0:
        session = db.connect()
        areas = session.query(model.Area)
        for area in areas:
            gene_controller = area.gene_controller.first()
            if gene_controller.active is False:
                continue
            else:
                gene_controller.nitrogen_LT_status = get_random_value()
                session.add(gene_controller)
                session.commit()

        session.close()
    else:
        return
