import jwt
import os
import db
import model
from fastapi import Response, HTTPException, Cookie, status
from typing import Optional

SECRET = os.environ.get('JWT_SECRET') or 'test'

responses = {
    status.HTTP_403_FORBIDDEN: {
        'description': "Authentication error: Token cookie not provided, Invalid JWT signature or User not found"
    },
}


async def authenticate(response: Response, token: Optional[str] = Cookie(None)) \
        -> (model.User, db._SessionCreator):
    if not token:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Token cookie not provided")
    try:
        auth_data = jwt.decode(
            token, key=SECRET,
            algorithms=['HS256']
        )
    except jwt.exceptions.InvalidSignatureError:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Invalid JWT signature")
    session = db.connect()
    user = session \
        .query(model.User) \
        .filter_by(id=auth_data['id']) \
        .first()
    if not user:
        session.close()
        raise HTTPException(status.HTTP_403_FORBIDDEN, "User Authentication error")
    # update cookie
    await create_token(response, user)
    return user, session


async def create_token(
        response: Response,
        user: model.User,
):
    payload = {
        'id': user.id,
        'email': user.email,
        'name': user.name,
    }
    await set_token_cookie(
        response,
        value=jwt.encode(
            payload=payload,
            key=SECRET,
        ),
    )
    return payload


async def set_token_cookie(response: Response, value):
    response.set_cookie(
        key='token',
        path='/',
        secure=os.environ.get('ENV') == 'production',
        samesite="None" if os.environ.get('ENV') == 'production' else "lax",
        domain=".egrid.com.br" if os.environ.get('ENV') == 'production' else None,
        max_age=30 * 24 * 60 * 60,  # 30 days expiration
        value=value,
    )
