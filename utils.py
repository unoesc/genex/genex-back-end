import random


def get_random_string(length, characters: str = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"):
    # Random string with the combination of upper case and numbers
    # removed easily confused characters (I1O0)
    result_str = ''.join(random.choice(characters) for _ in range(length))
    return result_str
