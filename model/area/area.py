from sqlalchemy.orm import relationship
from .. import Base
from sqlalchemy import Column, String, Integer, ForeignKey


# Propriedade
class Area(Base):
    __tablename__ = 'Area'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    name = Column(String(100), nullable=False)
    description = Column(String(200), nullable=True)

    # Animais da propriedade
    animals = relationship("Animal", lazy="dynamic", cascade="all, delete-orphan")

    # Controle de sêmis da propriedade, se ela tiver sêmis próprios
    gene_controller = relationship("GeneController", lazy="dynamic", cascade="all, delete-orphan")
