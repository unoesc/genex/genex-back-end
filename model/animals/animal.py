from sqlalchemy.orm import relationship
from .. import Base
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from datetime import datetime


class Animal(Base):
    __tablename__ = 'Animal'

    id = Column(String(50), primary_key=True)
    area_id = Column(Integer, ForeignKey("Area.id"), nullable=False)
    created_datetime = Column(DateTime, default=datetime.now, nullable=False)

    name = Column(String(100), nullable=True)

    history = relationship("AnimalHistory", lazy="dynamic", cascade="all, delete-orphan")

