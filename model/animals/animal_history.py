import enum
from sqlalchemy.orm import relationship
from .. import Base
from sqlalchemy import Column, String, Integer, ForeignKey, Enum, DateTime
from datetime import datetime


class AnimalHistory(Base):
    __tablename__ = 'AnimalHistory'

    animal_id = Column(String(50), ForeignKey("Animal.id"), primary_key=True)
    created_datetime = Column(DateTime, primary_key=True, default=datetime.now)

    class Status(str, enum.Enum):
        unknown = "unknown"
        inseminated = "inseminated"
        pregnant = "pregnant"
        lactation = "lactation"
        dry = "dry"
        repeating = "repeating"
        in_heat = "in_heat"
        missed_fertilization = "missed_fertilization"
        fattening = "fattening"

        @classmethod
        def to_portuguese(cls, status: str):
            if status == cls.unknown:
                return "Desconhecido"
            elif status == cls.inseminated:
                return "Inseminada"
            elif status == cls.pregnant:
                return "Em gestação"
            elif status == cls.lactation:
                return "Lactante"
            elif status == cls.dry:
                return "Seca"
            elif status == cls.repeating:
                return "Repetindo"
            elif status == cls.in_heat:
                return "No cio"
            elif status == cls.missed_fertilization:
                return "Cria perdida"
            elif status == cls.fattening:
                return "Na engorda"
            else:
                return "Desconhecido"

        @classmethod
        def from_portuguese(cls, asset: str):
            if asset == "Desconhecido":
                return cls.unknown
            elif asset == "Inseminada":
                return cls.inseminated
            elif asset == "Em gestação":
                return cls.pregnant
            elif asset == "Lactante":
                return cls.lactation
            elif asset == "Seca":
                return cls.dry
            elif asset == "Repetindo":
                return cls.repeating
            elif asset == "No cio":
                return cls.in_heat
            elif asset == "Cria perdida":
                return cls.missed_fertilization
            elif asset == "Na engorda":
                return cls.fattening
            else:
                return cls.unknown

    status = Column(Enum(Status), nullable=False, default=Status.unknown)

    private_semi_id = Column(Integer, ForeignKey("Semi.id"), nullable=True)
    private_semi = relationship("Semi")

    public_semi_bull_id = Column(String(50), nullable=True)
    public_semi_breed = Column(String(100), nullable=True)

