from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from .users.user import *
from .gene.semi import *
from .gene.gene_controller import *
from .animals.animal import *
from .animals.animal_history import *
from .area.area import *
