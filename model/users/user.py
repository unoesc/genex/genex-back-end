from sqlalchemy.orm import relationship
from .. import Base
from sqlalchemy_utils import PasswordType
from sqlalchemy import Column, String, Integer, DateTime
from datetime import datetime


class User(Base):
    __tablename__ = 'User'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    email = Column(String(50), unique=True, nullable=False)
    phone = Column(String(50), nullable=True)
    password = Column(PasswordType(schemes='bcrypt'), nullable=False)
    created_datetime = Column(DateTime, nullable=False, default=datetime.now)

    areas = relationship("Area", lazy="dynamic", cascade="all, delete-orphan")
