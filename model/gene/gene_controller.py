from sqlalchemy.orm import relationship
from .. import Base
from sqlalchemy import Column, Integer, ForeignKey, Boolean, Date, String, Float
from datetime import date


class GeneController(Base):
    __tablename__ = 'GeneController'

    area_id = Column(Integer, ForeignKey("Area.id"), primary_key=True)
    active = Column(Boolean, nullable=False, default=False)
    sensor_token = Column(String(10), nullable=False, unique=True)

    nitrogen_LT_status = Column(Float, nullable=False, default=0)
    nitrogen_day_loss_tax = Column(Float, nullable=False, default=0)

    nitrogen_next_refill = Column(Date, nullable=True)
    nitrogen_last_refill = Column(Date, nullable=True)

    semis = relationship("Semi", lazy="dynamic", cascade="all, delete-orphan")

