from .. import Base
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from datetime import datetime


class Semi(Base):
    __tablename__ = 'Semi'

    id = Column(Integer, primary_key=True, autoincrement=True)
    gene_controller_id = Column(Integer, ForeignKey("GeneController.area_id"), nullable=False)

    bull_id = Column(String(50), nullable=False)
    breed = Column(String(100), nullable=False)

    acquisition_datetime = Column(DateTime, nullable=False, default=datetime.now)
    used_datetime = Column(DateTime, nullable=True)
