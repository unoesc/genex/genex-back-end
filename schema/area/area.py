from .. import BaseModel, GeneControllerPost, GeneControllerGet
from typing import Optional


class PostArea(BaseModel):
    name: str
    description: Optional[str]
    private_gene: Optional[GeneControllerPost]


class AreaGet(BaseModel):
    name: str
    description: Optional[str]
    id: int
    animals_count: int
    gene_activated: bool
    private_gene: Optional[GeneControllerGet]

    @classmethod
    def from_orm(cls, o, **kwargs):
        return super().from_orm(
            o,
            animals_count=o.animals.count(),
            gene_activated=o.gene_controller.first().active if o.gene_controller.first() is not None else False,
            private_gene=GeneControllerGet.from_orm(o.gene_controller.first()) if o.gene_controller.first() is not None else None,
            **kwargs
        )


class AreaPatch(PostArea):
    id: int
