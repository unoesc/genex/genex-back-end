from datetime import date
from typing import List
from . import BaseModel
from .gene.semi import SemisGet


class GetReport(BaseModel):
    cows: int
    min_date: date
    repetitions: int
    inseminations: int
    missed_fertilizations: int
    pregnants: int
    cow_drys: int
    cow_lactations: int
    cow_fattening: int
    used_semis: List[SemisGet]
