import pydantic
from typing import List


class BaseModel(pydantic.BaseModel):

    def extract_data_to(self, data, ignore: list = None):
        if ignore is None:
            ignore = []
        for field in self.__fields__:
            if field not in ignore:
                data.__setattr__(field, self.__getattribute__(field))

    def to_dict(self):
        return dict(self)

    @classmethod
    def from_orm(cls, o, **kwargs):
        data = {
            field: o.__getattribute__(field)
            for field in cls.__fields__
            if field not in kwargs.keys()
        }
        return cls(**data, **kwargs)


class MonthYearModel(BaseModel):
    month: int
    year: int


class CategoriesModel(BaseModel):
    id: int
    variables: List[str]


from .users.user import *
from .gene.gene_controller import *
from .area.area import *
from .gene.semi import *
from .animals.animal import *
from .animals.animal_history import *
from .reports import *


