import model
from .. import BaseModel
from typing import Optional
from datetime import date


class GeneControllerPost(BaseModel):
    nitrogen_LT_status: float
    nitrogen_day_loss_tax: float

    nitrogen_next_refill: Optional[date]
    nitrogen_last_refill: date


class GeneControllerPatch(BaseModel):
    area_id: int
    nitrogen_LT_status: float
    nitrogen_day_loss_tax: float

    nitrogen_next_refill: date


class GeneControllerGet(BaseModel):
    active: bool
    sensor_token: str
    nitrogen_LT_status: Optional[float]
    nitrogen_day_loss_tax: Optional[float]

    nitrogen_next_refill: Optional[date]
    nitrogen_last_refill: Optional[date]

    nitrogen_current_status: Optional[float]
    available_semis: Optional[int]

    @classmethod
    def from_orm(cls, o, **kwargs):
        current_status = 0
        if o.active:
            current_status = o.nitrogen_LT_status - (
                        (date.today() - o.nitrogen_last_refill).days * o.nitrogen_day_loss_tax)

        return super().from_orm(
            o,
            nitrogen_current_status=current_status,
            available_semis=o.semis.filter(model.Semi.used_datetime == None).count(),
            **kwargs
        )
