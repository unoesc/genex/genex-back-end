import model
from .. import BaseModel
from typing import Optional, List
from datetime import datetime


class Semi(BaseModel):
    id: int
    breed: str
    bull_id: str
    acquisition_datetime: datetime
    used_datetime: Optional[datetime]


class SemisGet(BaseModel):
    group_name: str
    semis: List[Semi]

    @classmethod
    def custom_from_orm(cls, group_name: str, semis: List[model.Semi], **kwargs):
        return super().from_orm(
            None,
            group_name=group_name,
            semis=[Semi.from_orm(semi) for semi in semis],
            **kwargs
        )


class PostSemis(BaseModel):
    area_id: int
    quantity: int
    breed: str
    bull_id: str

