import model
from .. import BaseModel
from typing import Optional, List


class Animal(BaseModel):
    area_id: int
    id: str
    name: Optional[str]


class GetAnimal(Animal):
    current_status: str

    @classmethod
    def custom_from_orm(cls, o, **kwargs):
        return super().from_orm(
            o,
            current_status=model.AnimalHistory.Status.to_portuguese(o.history.order_by(
                model.AnimalHistory.created_datetime.desc()
            ).first().status),
            **kwargs
        )


class GetAnimals(BaseModel):
    animals: List[GetAnimal]
    filters: List[str]

    @classmethod
    def custom_from_orm(cls, o, filters, current_status, **kwargs):
        animals = []
        if current_status is not None:
            for animal in o:
                if animal.history.order_by(
                        model.AnimalHistory.created_datetime.desc()
                ).first().status == model.AnimalHistory.Status.from_portuguese(current_status):
                    animals.append(animal)
        else:
            animals = o

        return super().from_orm(
            o,
            animals=[GetAnimal.custom_from_orm(animal) for animal in animals],
            filters=filters,
            **kwargs
        )


class AnimalsFilter(BaseModel):
    area_id: int
    current_status: Optional[str]
    text: Optional[str]
