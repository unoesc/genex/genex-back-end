import model
import schema
from .animal import GetAnimal
from .. import BaseModel
from typing import Optional, List
from datetime import datetime, date


class GetHistory(BaseModel):
    animal_id: str
    created_datetime: datetime
    status: str
    private_semi: Optional[schema.Semi]
    public_semi_breed: Optional[str]
    public_semi_bull_id: Optional[str]

    @classmethod
    def from_orm(cls, animal_history, **kwargs):
        return super().from_orm(
            animal_history,
            **kwargs,
            status=model.AnimalHistory.Status.to_portuguese(animal_history.status),
            private_semi=schema.Semi.from_orm(animal_history.private_semi) if animal_history.private_semi is not None else None,
            public_semi_breed=animal_history.public_semi_breed if animal_history.public_semi_breed is not None else None,
            public_semi_bull_id=animal_history.public_semi_bull_id if animal_history.public_semi_bull_id is not None else None,
        )


class GetHistories(BaseModel):
    histories: List[GetHistory]
    filters: List[str]
    initial_date: datetime

    @classmethod
    def custom_from_orm(cls, animal_histories, initial_date, filters, **kwargs):
        return super().from_orm(
            animal_histories,
            filters=filters,
            initial_date=initial_date,
            histories=[GetHistory.from_orm(animal_history) for animal_history in animal_histories],
            **kwargs,
        )


class HistoryFilter(BaseModel):
    animal_id: str
    start_date: Optional[date]
    status: Optional[str]


class AlterHistory(BaseModel):
    area_id: int
    animal_id: str
    status: str
    private_semi_id: Optional[int]
    public_semi_breed: Optional[str]
    public_semi_bull_id: Optional[str]


class HistoryEvents(BaseModel):
    title: str
    type: str
    occurrence_date: Optional[date]
    text: Optional[str]
    animal: Optional[GetAnimal]
