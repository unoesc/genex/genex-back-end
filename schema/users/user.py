from .. import BaseModel
from typing import Optional


class PostUser(BaseModel):
    email: str
    password: str
    name: str
    phone: Optional[str]


class LoginUser(BaseModel):
    email: str
    password: str


class GetUser(BaseModel):
    email: str
    name: str
    phone: Optional[str]


class UserPatch(BaseModel):
    name: Optional[str]
    phone: Optional[str]
    email: Optional[str]


