from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
import os

engine = None
_SessionCreator = None


def connect_db():
    global engine, _SessionCreator
    username = 'Inbov'
    password = 'Inbov_1234'
    database = 'inbov'
    server = 'localhost'
    port = '3306'
    engine = create_engine(
        f'mysql+pymysql://{username}:{password}@{server}:{port}/{database}',
        echo_pool=os.environ.get('SQL_ECHO_POOL', "").lower() == 'true' or False,
        pool_pre_ping=os.environ.get('SQL_POOL_PRE_PING', "").lower() == 'true' or False,
        max_overflow=int(os.environ.get('SQL_MAX_OVERFLOW', 5)),
        pool_size=int(os.environ.get('SQL_POOL_SIZE', 5)),
        pool_recycle=int(os.environ.get('SQL_POOL_RECYCLE', -1)),
    )
    _SessionCreator = sessionmaker(bind=engine)


connect_db()


def connect() -> Session:
    return _SessionCreator()
