import model
import db

model.Base.metadata.drop_all(db.engine)
model.Base.metadata.create_all(db.engine)

session = db.connect()

session.commit()
session.close()
